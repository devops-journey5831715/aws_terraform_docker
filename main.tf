provider "aws" {
  region = "eu-central-1"  # Choose the region you prefer
}

resource "aws_instance" "testserver" {
  ami           = "ami-023adaba598e661ac"  # Example AMI, replace with a valid one
  instance_type = "t2.micro"
  key_name      = "nVIRGINIA"  # Optional: Specify your key pair name

# Include user data script
  user_data = file("${path.module}/config.sh")

  tags = {
    Name = "TestServer"
  }
}

output "public_ip" {
  value = aws_instance.testserver.public_ip
}

